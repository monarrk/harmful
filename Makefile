CC = clang

all: gethost tinyweb getbat

getbat: getbat.c
	${CC} -o getbat -lprop getbat.c

gethost: gethost.c
	${CC} -o gethost gethost.c 

tinyweb: tinyweb.c
	${CC} -o tinyweb tinyweb.c

clean:
	rm -f *.core getbat gethost tinyweb

fmt:
	clang-format -i *.c
