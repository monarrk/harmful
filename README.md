# harmful
my personal utilities for doing things

many of these are likely specific to NetBSD, but it's possible that they're portable. portability, however, is not a goal (as of now!!)

### files
- getbat : get battery percentage
- gethost : get IP address from host
- tinyweb : a itty bitty tiny web server
- img.sh : autoindex images/videos into html

### building
what you'll need:
 - BSD make
 - clang

then, just `make`.
