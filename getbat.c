#include <fcntl.h>
#include <prop/proplib.h>
#include <stdio.h>
#include <string.h>
#include <sys/envsys.h>
#include <sys/ioctl.h>
#include <sys/stat.h>

int main() {
	prop_dictionary_t dict = prop_dictionary_create();

	// Open the sysmon file to read from
	int fd = open("/dev/sysmon", O_RDONLY, 0640);
	if (fd == -1) {
		printf("Error opening /dev/sysmon\n");
		return fd;
	}

	// Read the dictionary from sysmon with the ENVSYS_GETDICTIONARY ioctl
	int error = prop_dictionary_recv_ioctl(fd, ENVSYS_GETDICTIONARY, &dict);
	if (error == -1) {
		printf("Error receiving from IOCTL: %d\n", error);
		return error;
	}

	prop_object_t obj;
	prop_object_iterator_t iter = prop_dictionary_iterator(dict);

	// Iterate over all devices
	while ((obj = prop_object_iterator_next(iter)) != NULL) {
		prop_array_t array = prop_dictionary_get_keysym(dict, obj);
		prop_object_iterator_t siter = prop_array_iterator(array);
		prop_object_t sobj;
		// Iterate over every attribute
		while ((sobj = prop_object_iterator_next(siter)) != NULL) {
			const char* desc = NULL;
			if (prop_dictionary_get_cstring_nocopy(sobj, "description", &desc) == false)
				continue;
			// Find the `charge` attribute
			if (strcmp("charge", desc) == 0) {
				prop_object_t bat = prop_dictionary_get(sobj, "cur-value");
				prop_object_t max = prop_dictionary_get(sobj, "max-value");
				// Make sure cur-value and max-value exist
				if (bat && max) {
					// Find percentage in Ah
					double bat_val = (double)prop_number_integer_value(bat);
					double max_val = (double)prop_number_integer_value(max);
					printf("%.2f%%\n", ((bat_val)*100.0) / max_val);
				}
			}
		}
	}
}
