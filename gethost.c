// adapted from Hacking: The Art of Exploitation

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

#define eprintf(fmt, x) fprintf(stderr, fmt, x)

int main(int argc, char** argv) {
	if (argc < 2) {
		eprintf("Usage: %s <hostname>\n", argv[0]);
		exit(1);
	}

	struct hostent* host_info = gethostbyname(argv[1]);

	if (host_info == NULL) {
		eprintf("Couldn't lookup %s\n", argv[1]);
	} else {
		struct in_addr* address = (struct in_addr*)(host_info->h_addr);
		printf("%s\n", inet_ntoa(*address));
	}
}
