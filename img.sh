#!/bin/sh

usage() {
	echo "img.sh: autoindex media files"
	echo "synopsis: img.sh [directory to index] [template file]"
	exit 0
}

INDEX=""
if [ -n "$1" ]; then
	INDEX="${1}"
else
	usage
fi

NAME=""
if [ -n "$2" ]; then
	NAME="${2}"
else
	usage
fi

# temp var
RESULT="<div id=\"imgsh-images\">"

# generate html
for d in "${INDEX}"/*; do
	# full path
	P="/$(basename "${INDEX}")/$(basename "${d}")"

	ELEM="img"
	# use <video> for mp4s
	if file --mime-type "${d}" | grep -q mp4$; then
		ELEM="video"
	fi

	# skip txt files
	if file --mime-type "${d}" | grep -q "text/plain"; then continue; fi

	# get a description
	DESCFILE="${d}/.txt"
	DESC=""
	[ -e "${DESCFILE}" ] && DESC="<p id=\"${DESCFILE}-desc\" class=\"description\">$(cat "${DESCFILE}")</p>"

	# concat the html
	RESULT="${RESULT}
	<div class=\"img-wrap\">
		<h1>${P}</h1>
		${DESC}
		<a href=\"${P}\" target=\"_blank\"><${ELEM} src=\"${P}\"/></a>
	</div>
	<br/><br/><br/>"
done

# finish div elem
RESULT="${RESULT}
</div>"

# use perl because sed doesn't like html
PATTERN="${RESULT}" perl -p -e 's/{{IMAGES}}/$ENV{PATTERN}/g' "${NAME}"
