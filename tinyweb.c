#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#define PORT 80

#define EOL "\r\n"
#define EOL_SIZE 2

#define matches(a, b) (strcmp((a), (b)) == 0)
#define fatal(x)                            \
	{                                       \
		fprintf(stderr, "ERROR!: %s\n", x); \
		exit(1);                            \
	}

void handle_connection(int sockfd, struct sockaddr_in* client_addr_ptr, const char* webroot);
int get_file_size(int fd);
int send_string(int sockfd, char* buffer);
int recv_line(int sockfd, char* dest_buffer);

// get the size of a file
int get_file_size(int fd) {
	struct stat stat_struct;

	if (fstat(fd, &stat_struct) == -1) {
		return -1;
	}
	return (int)stat_struct.st_size;
}

// accepts a socket fd and a ptr to the null terminated string to send
// the function will make sure all the bytes of the string are sent
// returns 1 on success and 0 on failure
int send_string(int sockfd, char* buffer) {
	int sent_bytes, bytes_to_send;

	bytes_to_send = strlen(buffer);

	while (bytes_to_send > 0) {
		sent_bytes = send(sockfd, buffer, bytes_to_send, 0);
		if (sent_bytes == -1)
			return 0;
		bytes_to_send -= sent_bytes;
		buffer += sent_bytes;
	}

	// success!
	return 1;
}

// accepts a socket fd and a ptr to a buffer. it will receive from the socket
// until the EOL byte sequence is seen. the EOL bytes are read from the socket,
// but the destination buffer is terminated before these bytes.
// returns the size of the read line
int recv_line(int sockfd, char* dest_buffer) {
	char* ptr;
	int eol_matched = 0;

	ptr = dest_buffer;

	// read a single byte
	while (recv(sockfd, ptr, 1, 0) == 1) {
		// does this byte match the terminator?
		if (*ptr == EOL[eol_matched]) {
			eol_matched++;

			// if all bytes match the terminator
			if (eol_matched == EOL_SIZE) {
				*(ptr + 1 - EOL_SIZE) = '\0';
				return strlen(dest_buffer);
			}
		} else {
			eol_matched = 0;
		}

		// increment to the next byte
		ptr++;
	}

	// didn't find the EOL chars
	return 0;
}

// handles the web connection on the passed socket from the passed client
// address. The connection is processed as a web request, and this function
// replies over the socket
void handle_connection(int sockfd, struct sockaddr_in* client_addr_ptr, const char* webroot) {
	char *ptr, request[500], resource[500];
	int fd, length;

	length = recv_line(sockfd, request);

	printf("Got request from %s:%d \"%s\"\n", inet_ntoa(client_addr_ptr->sin_addr), ntohs(client_addr_ptr->sin_port), request);

	// search for valid-looking request
	ptr = strstr(request, " HTTP/");

	if (ptr == NULL) {
		printf("not HTTP!\n");
	} else {
		// terminate the buffer at the end of the URL
		*ptr = 0;
		// set ptr to NULL (used to flag for an invalid address)
		ptr = NULL;

		// a GET request
		if (strncmp(request, "GET ", 4) == 0)
			ptr = request + 4;

		// a HEAD request
		if (strncmp(request, "HEAD ", 5) == 0)
			ptr = request + 5;

		// invalid request
		if (ptr == NULL) {
			printf("\tUNKNOWN REQUEST TYPE\n");
		} else {
			// for resources ending in '/', add index.html to the end
			if (ptr[strlen(ptr) - 1] == '/')
				strcat(ptr, "index.html");

			// begin join with root path
			strcpy(resource, webroot);
			// and join with resource path
			strcat(resource, ptr);

			// try to open the file
			fd = open(resource, O_RDONLY, 0);
			printf("\topening %s\t", resource);
			if (fd == -1) {
				printf("404 Not Found\n");
				send_string(sockfd, "HTTP/1.0 404 NOT FOUND\r\n");
				send_string(sockfd, "Server: tinyweb (https://gitlab.com/monarrk/harmful)\r\n\r\n");
				send_string(sockfd, "<html><head><title>404 Not Found</title></head>");
				send_string(sockfd, "<body><h1>Error 404: URL Not Found.</h1></body></html>\r\n");
			} else {
				// send up the file

				send_string(sockfd, "HTTP/1.0 200 OK\r\n");
				send_string(sockfd, "Server: tinyweb (https://gitlab.com/monarrk/harmful)\r\n\r\n");

				// this is a valid get request
				if (ptr == request + 4) {
					if ((length = get_file_size(fd)) == -1)
						fatal("failed to get resource file size");
					if ((ptr = (char*)malloc(length)) == NULL)
						fatal("failed to allocate memory for reading resource");

					// read the file into memory
					read(fd, ptr, length);

					// send it to the socket
					send(sockfd, ptr, length, 0);

					// free the file memory
					free(ptr);
				}
				close(fd);
			}
		}
	}

	// close the socket
	shutdown(sockfd, SHUT_RDWR);
}

int help() {
	printf("Usage: tinyweb [OPTIONS]"
""
"OPTIONS:"
"    --root    set the webroot (defaults to /srv/www/tiny)");
	return 1;
}

int main(int argc, char** argv) {
	int sockfd, new_sockfd, yes = 1;
	struct sockaddr_in host_addr, client_addr;
	socklen_t sin_size;

	char* webroot = "/srv/www/tiny";

	// parse arguments
	for (int i = 0; i < argc; i++) {
		if (matches(argv[i], "--root")) {
			if ((i + 1) < argc) {
				i++;
				webroot = argv[i + 1];
			} else {
				return help();
			}
		}
	}

	printf("Accepting web requests on port %d\n", PORT);

	if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1)
		fatal("failure in socket");
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
		fatal("failure setting socket option SO_REUSEADDR");

	host_addr.sin_family = AF_INET; // host byte order
	host_addr.sin_port = htons(PORT); // short, network byte order
	host_addr.sin_addr.s_addr = INADDR_ANY; // automatically fill ip
	memset(&(host_addr.sin_zero), '\0', 8); // zero the rest of the struct

	if (bind(sockfd, (struct sockaddr*)&host_addr, sizeof(struct sockaddr)) == -1)
		fatal("failure binding to socket");
	if (listen(sockfd, 20) == -1)
		fatal("unable to listen to socket");

	// accept loop
	while (1) {
		sin_size = sizeof(struct sockaddr_in);
		new_sockfd = accept(sockfd, (struct sockaddr*)&client_addr, &sin_size);
		if (new_sockfd == -1)
			fatal("failure accepting connection");

		handle_connection(new_sockfd, &client_addr, webroot);
	}

	return 0;
}
